## Test results frun from "tests" folder as of 2021-03-17 

Below are [Emily's 3 tests ](https://gitlab.oit.duke.edu/aeh-group/fastapi-html-sec-struct/-/issues/3#note_140260)
We'll call these 3 tests demo4*, demo5*, demo6*.

These tests were run with a newer version of the python program from an email on 2021-03-11 and renamed `RBIND_2ndS.py`. The python script is called from the create-ct.sh script. And the python script reads a newer version of spreadsheet `RBIND_2ndS.xls` which instead of NxN entries, like `1x1`, now uses N,M entries like `1,1`

---

### demo4 CCAGAUUUGAGCCUGGAGCUCUCUGG
- should form a 3-nt bulge
```
cd  /Users/Shared/gitrepos/rbind-2nd-struc/tests
bash-5.0$ ./create-ct.sh $(cat demo4-hiv-1-tar.seq) 
CCAGAUUUGAGCCUGGAGCUCUCUGG
(((((...((((.....))))))))) ( -9.40)
There is(are) 1 apical loop(s)
There is(are) 1 bulge loop(s)
There is(are) 0 internal loop(s)
There is(are) 0 linker(s)
whats size of apical loop [5]
whats size of bulge loop [3]
The following molecule(s) in RBIND potentially bind to bulge loop of this RNA.
         Database ID RNA Secondary Structure (Binding Location) size of loop sequence
15  R-BIND (SM) 0049                                     Bulge*            3      UCU
16  R-BIND (SM) 0058                                      Bulge            3      UCU
17  R-BIND (SM) 0059                                      Bulge            3      NaN
The following molecule(s) in RBIND potentially bind to apical loop of this RNA.
         Database ID RNA Secondary Structure (Binding Location) size of loop sequence
8   R-BIND (SM) 0040                                      Bulge            5    AACUA
9   R-BIND (SM) 0041                                      Bulge            5    AACUA
10  R-BIND (SM) 0042                                      Bulge            5    AACUA
11  R-BIND (SM) 0043                                      Bulge            5    AACUA
12  R-BIND (SM) 0044                                      Bulge            5    AACUA
no molecules in RBIND bind to internal loop of this RNA.
```

---
### demo5 CUG repeats (12): CUGCUGCUGCUGCUGCUGCUGCUGCUGCUGCUGCUG
- should have 1x1 internal loops
```
bash-5.0$ ./create-ct.sh $(cat demo5-cug12.seq)
CUGCUGCUGCUGCUGCUGCUGCUGCUGCUGCUGCUG
..((.((.((.((.((....)).)).)).)).)).. (-13.10)
There is(are) 1 apical loop(s)
There is(are) 0 bulge loop(s)
There is(are) 4 internal loop(s)
There is(are) 0 linker(s)
whats size of apical loop [4]
whats size of bulge loop []
no bulge loop in this RNA.
The following molecule(s) in RBIND potentially bind to apical loop of this RNA.
         Database ID RNA Secondary Structure (Binding Location) size of loop sequence
27  R-BIND (SM) 0072                                Apical Loop            4     GGAG
whats(are) size of internal loop(s): [[1, 1], [1, 1], [1, 1], [1, 1]]
The following molecule(s) in RBIND potentially bind to the [1, 1] internal loop of this RNA.
         Database ID RNA Secondary Structure (Binding Location) size of loop sequence
5  R-BIND (SM) 0027A                              Internal Loop          1,1      G,G
The following molecule(s) in RBIND potentially bind to the [1, 1] internal loop of this RNA.
         Database ID RNA Secondary Structure (Binding Location) size of loop sequence
5  R-BIND (SM) 0027A                              Internal Loop          1,1      G,G
The following molecule(s) in RBIND potentially bind to the [1, 1] internal loop of this RNA.
         Database ID RNA Secondary Structure (Binding Location) size of loop sequence
5  R-BIND (SM) 0027A                              Internal Loop          1,1      G,G
The following molecule(s) in RBIND potentially bind to the [1, 1] internal loop of this RNA.
         Database ID RNA Secondary Structure (Binding Location) size of loop sequence
5  R-BIND (SM) 0027A                              Internal Loop          1,1      G,G
```
---
### demo6 
- should have 2x2 internal loops
```bash
bash-5.0$ ./create-ct.sh $(cat demo6-ccug31.seq)
CCUGCCUGCCCUGCCUGCCUGCCUGCCUGCCUGCCUGCCUG
...((..((...((..((......))..))..))..))... ( -4.40)
There is(are) 1 apical loop(s)
There is(are) 0 bulge loop(s)
There is(are) 3 internal loop(s)
There is(are) 0 linker(s)
whats size of apical loop [6]
whats size of bulge loop []
no bulge loop in this RNA.
The following molecule(s) in RBIND potentially bind to apical loop of this RNA.
no molecules in RBIND bind to apical loop of this RNA.
whats(are) size of internal loop(s): [[2, 2], [3, 2], [2, 2]]
The following molecule(s) in RBIND potentially bind to the [2, 2] internal loop of this RNA.
no molecules in RBIND bind to [2, 2] internal loop of this RNA.
The following molecule(s) in RBIND potentially bind to the [3, 2] internal loop of this RNA.
no molecules in RBIND bind to [3, 2] internal loop of this RNA.
The following molecule(s) in RBIND potentially bind to the [2, 2] internal loop of this RNA.
no molecules in RBIND bind to [2, 2] internal loop of this RNA.
```
