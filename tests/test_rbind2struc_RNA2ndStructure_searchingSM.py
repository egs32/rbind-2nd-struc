#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 01:06:14 2020

@author: leonchiu
"""
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import xlrd
import string
import re
fs = []
rs = []
with open('connect.ct') as fobj:
    for line in fobj:
        row = line.split()
        fs.append(row[:-1])
        rs.append(row[-1])
#print (data)
#print (target)

# count how many residues in the connect file #
frame = "connect.ct"
count = 0
with open(frame, 'r') as f:
    for line in f:
        count += 1
#print("Total number of lines is:", count)
# L is used to count how many linkers #
residue = []
PairR = []
i=0
c=0
L=0

#BL count how many bulge loop in the system #  
#IN count how many internal loop in the system # 
BL=0
IN=0
AP=0
FinIN=0
# save nts size of bulge/internal loop into list, BLs, INs#
BLs_list = []
INs_list = []
APs_list = []


for x in range (0,count):

# extract list from the data list, in order to get the number #
    
    residue = fs[x]
    z=residue[0]
    h=int(z)
    PairR = rs[x]
    y = int(rs[x])
    b = int (rs[x-1])
 #   print (b)
#    print (residue)
    if y == 0: 
#        print (z, 'is not base pairing')
        # check the other side, to see whether it is base pairing, if yes, then bulge; if not, then internal loop#
        minusy=int(rs[x-1])
#        print (minusy)
        minusy1=minusy-1
#        print (minusy1)
### the extra minusy1-1 is because the list or string start at 0 ###        
#        print ('rs[minusy1]',rs[minusy1-1])
        a= int(rs[minusy1-1])
# a is 
#        print ('a is',a)
# i is for counting the nts size of bulge or internal loop #           
        if b == 0:
                   
                   if c == 0:
#                       print (z,'it is internal loop')
                       i=i+1
                   else:
#                       print (z,'it is bulge loop')
                       i=i+1
        else:
                   if a == 0:
                       c=0
#c is a marker to give same marker of sequential residues located in the same loop#                       
#                       print (z,'it is internal loop')
                       i=i+1
                   else:
                       c=1
 #                      print (z,'it is bulge loop')
                       i=i+1
    else:
        if i>0:           
#BL count how many bulge loop in the system #  
#IN count how many internal loop in the system #              
            if c == 0:
                    if h == y+i+1:
                       AP=AP+1 
                       g=i
                       APs_list.append(i)
#                       print ('the apical loop size is' ,i, 'nts')
                       i=0
                    else:
                       d=i
                       IN=IN+1
                       INs_list.append(d)
#                       i=0
                       if (x+1 > y and x-d > int(rs[x-d-1])):
                           i=0
                       elif (x+1 < y and x-d < int(rs[x-d-1])):
                           i=0
                       else:
#                           print(x+1)
#                           print(y)
#                           print(x-d)
#                           print(int(rs[x-d-1]))                          
                           IN=IN-1
                           L=L+1
                           del INs_list[-1]
                           i=0
            else:
                BL=BL+1
                e=i
#                print(x)
                BLs_list.append(e)
#                print ('the bulge loop size is' ,i, 'nts')
                i=0
        else:
#            print (z, 'is base pair with',y) 
            i=0


######  remove the first element in the list if N-termianl starts from singal strand  ######            
if int(rs[0])==0:
#                del INs_list[0]
                L=L-1    
else:
                pass
######  remove the first element in the list if N-termianl starts from singal strand  ###### 
                
FinIN = IN/2
print ('There is(are)' ,AP, 'apical loop(s)')
print ('There is(are)' ,BL, 'bulge loop(s)')
print ('There is(are)' ,int(FinIN), 'internal loop(s)')
print ('There is(are)' ,L, 'linker(s)')
print ('whats size of apical loop', APs_list)
print ('whats size of bulge loop', BLs_list)
#print ('whats size of internal loop', INs_list)

df = pd.read_excel('/Users/leonchiu/Desktop/Desktop_Liang/CWRU_home/Lab/RBIND/ReadSequence/RBIND_2ndS.xlsx', sheet_name='Sheet1')

#j=0
#countAPs=len(APs_list)
#
#for x in range (0,countAPs):
#    j=APs_list[x]
#    print (j)
#

#l=0
#countINs=len(INs_list)
#
#for x in range (0,countINs):
#    l=INs_list[x]
#    print (l)
k=0
countBLs=len(BLs_list)

if BL >0:    
            for x in range (0,countBLs):
                k=BLs_list[x]
#                print (k)
                subsetDataFrame = df[df['RNA Secondary Structure (Binding Location)'].isin(['Bulge']) ]
                filterinfDataframe = df[df['size of loop'] == k]
                print ('The following molecule(s) in RBIND potentially bind to bulge loop of this RNA.')
                if filterinfDataframe.empty:
                    print ('no molecules in RBIND bind to bulge loop of this RNA.')
                else:    
                    print (filterinfDataframe)
else:
                print ('no bulge loop in this RNA.')

j=0
countAPs=len(APs_list)

if AP >0:
            for x in range (0,countAPs):
                j=APs_list[x]
#                print (j)
                subsetDataFrame = df[df['RNA Secondary Structure (Binding Location)'].isin(['Apical Loop']) ]
                filterinfDataframe = df[df['size of loop'] == j]
                print ('The following molecule(s) in RBIND potentially bind to apical loop of this RNA.')
                if filterinfDataframe.empty:
                    print ('no molecules in RBIND bind to apical loop of this RNA.')
                else:    
                    print (filterinfDataframe)

else:
                print ('no apical loop in this RNA.')

##### internal Loop extraction, using the head-tail extraction from the INs_list  #####
##### Note: the excel format for the internal loop should follow this way A,B; C,D in excel sheet #####

if IN >0:

#            print('The count of element is:', len(INs_list))
            INa=len(INs_list)-1
            INb=len(INs_list)/2
#            print(INa)
#            print(INb)

            list1=[]
            list2=[]

            for i in range (0,int(INb)):
                list1.insert(i,[INs_list[i],INs_list[INa-i]])
                list2.insert(i,[INs_list[INa-i],INs_list[i]])
#                print(list1)
#                print(list2)
            print ('whats(are) size of internal loop(s):', list1)

                
            for i in range (0,int(INb)):
#                print('The internal loop is', list1[i])
#                print('The internal loop is', list2[i])
                string_ints1 = [str(int) for int in list1[i]]
                string_ints2 = [str(int) for int in list2[i]]
#                print(string_ints)
                str_of_ints1 = ",".join(string_ints1)
                str_of_ints2 = ",".join(string_ints2)
#                print(str_of_ints)
                filterinfDataframe1 = df[df['size of loop'].str.contains(str_of_ints1, na=False) | df['size of loop'].str.contains(str_of_ints2, na=False)]
#                filterinfDataframe2 = df[df['size of loop'].str.contains(str_of_ints2, na=False)]
                print ('The following molecule(s) in RBIND potentially bind to the', list1[i], 'internal loop of this RNA.')
                if filterinfDataframe1.empty:
                    print ('no molecules in RBIND bind to',list1[i] , 'internal loop of this RNA.')
                else:
                    print(filterinfDataframe1)
#    
    
else:
            print ('no molecules in RBIND bind to internal loop of this RNA.')
