[https://johannesgiorgis.com/sharing-conda-environments-across-different-operating-systems/](https://johannesgiorgis.com/sharing-conda-environments-across-different-operating-systems/) trick used to get py3vienna env from rhel79 rbind.chem.duke.edu working on macos 10.14 (homebrew-installed)
```bash
aeh-group$ conda env create -f environment-py3vienna-macos.yml 
Collecting package metadata (repodata.json): done
Solving environment: failed

ResolvePackageNotFound: 
  - libxcb=1.14
  - ld_impl_linux-64=2.33.1
  - secretstorage=3.1.2
  - patchelf=0.11
  - libgfortran-ng=7.3.0
  - libstdcxx-ng=9.1.0
  - libgcc-ng=9.1.0

```
So edit macos file and delete each line for each of above items (so they are only in the rhel79 file):
```
bash-5.0$ diff -y --suppress-common-lines  environment-py3vienna-rhel79.yml environment-py3vienna-macos.yml 
  - ld_impl_linux-64=2.33.1				      <
  - libgcc-ng=9.1.0					      <
  - libgfortran-ng=7.3.0				      <
  - libstdcxx-ng=9.1.0					      <
  - libxcb=1.14						      <
  - patchelf=0.11					      <
  - secretstorage=3.1.2					      <
bash-5.0 $ conda env create -f environment-py3vienna-macos.yml 
Collecting package metadata (repodata.json): done
Solving environment: -|       | 
Found conflicts! Looking for incompatible packages.
This can take several minutes.  Press CTRL-C to abort.
```